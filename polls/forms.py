from django import forms
from .models import Poll, Choice


class PollAddForm(forms.ModelForm):
    
    choice1 = forms.CharField(label='گزینه اول ', max_length=100, min_length=1,
                              widget=forms.TextInput(attrs={'class': 'form-control'}))
    choice2 = forms.CharField(label='گزینه دوم ', max_length=100, min_length=1,
                              widget=forms.TextInput(attrs={'class': 'form-control'}))
    poll_image = forms.ImageField(label='تصویر')

    class Meta:
        model = Poll
        fields = ['text', 'choice1', 'choice2','poll_image']
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control', 'rows': 5, 'cols': 20}),
        }


class EditPollForm(forms.ModelForm):
    class Meta:
        model = Poll
        fields = ['text', 'poll_image', ]
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control', 'rows': 5, 'cols': 20}),
        }


class ChoiceAddForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ['choice_text', ]
        widgets = {
            'choice_text': forms.TextInput(attrs={'class': 'form-control', })
        }
