from django import forms
from django.contrib.auth.models import User


class UserRegistrationForm(forms.Form):
    username = forms.CharField(label='نام کاربری ', max_length=100, min_length=3,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(label='نام ', max_length=100, min_length=3,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(label='نام خانوادگی ', max_length=100, min_length=3,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label='ایمیل ', max_length=20, min_length=3,
                             widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(label='رمز عبور ', max_length=50, min_length=3,
                                widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label='تکرار رمز عبور ',
                                max_length=50, min_length=3,
                                widget=forms.PasswordInput(attrs={'class': 'form-control'}))
