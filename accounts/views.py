from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from .forms import UserRegistrationForm
from django.contrib import messages
from django.http import HttpResponse


def login_user(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        
        if user is not None:
            login(request, user) # Attachs user to the current session
            request.session['user_id'] = request.user.id
            request.session['user_name'] = request.user.username
            if len(str(request.session['user_id'])) > 0 :
                redirect_url = request.GET.get('next', 'home')
                #print(f"Session did work! The user id in session is : {request.session['user_id']} and user name in session is : {request.session['user_name']}" )
                return redirect(redirect_url)
            else:
                print("Session didn't work!")
                return redirect('home')
        else:
            messages.error(request, "!نام کاربری یا رمز عبور اشتباه وارد شده است",
                           extra_tags='alert alert-warning alert-dismissible fade show')

    return render(request, 'accounts/login.html')


def logout_user(request):
    logout(request)
    return redirect('home')


def create_user(request):
    if request.method == 'POST':
        check1 = False
        check2 = False
        check3 = False
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            password1 = form.cleaned_data['password1']
            password2 = form.cleaned_data['password2']
            email = form.cleaned_data['email']

            if password1 != password2:
                check1 = True
                messages.error(request, '!تکرار رمز عبور نادرست است',
                               extra_tags='alert alert-warning alert-dismissible fade show')
            if User.objects.filter(username=username).exists():
                check2 = True
                messages.error(request, '!این نام کاربری قبلا انتخاب شده است',
                               extra_tags='alert alert-warning alert-dismissible fade show')
            if User.objects.filter(email=email).exists():
                check3 = True
                messages.error(request, '!شخص دیگری با این ایمیل قبلا ثبت نام کرده است',
                               extra_tags='alert alert-warning alert-dismissible fade show')

            if check1 or check2 or check3:
                messages.error(
                    request, "!خطا در ثبت نام. اطلاعات وارد شده را بررسی کنید", extra_tags='alert alert-warning alert-dismissible fade show')
                return redirect('accounts:register')
            else:
                user = User.objects.create_user(
                    username=username, first_name=first_name, last_name=last_name, password=password1, email=email)
                messages.success(
                    request, f'کاربر {user.username}! .ثبت نام شما انجام شد', extra_tags='alert alert-success alert-dismissible fade show')
                return redirect('accounts:login')
    else:
        form = UserRegistrationForm()
    return render(request, 'accounts/register.html', {'form': form})

def users_list(request):
    users = User.objects.all()
    return render(request, 'accounts/users.html', { 'users' : users })
